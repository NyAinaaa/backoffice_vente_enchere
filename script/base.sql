DROP DATABASE IF EXISTS vente_enchere;
CREATE DATABASE vente_enchere;
ALTER DATABASE vente_enchere OWNER TO pgsql_rojo;

\c vente_enchere

CREATE  TABLE categorie ( 
	id_categorie         smallserial  NOT NULL  ,
	libelle_categorie    varchar(50)  NOT NULL  ,
	CONSTRAINT pk_categorie PRIMARY KEY ( id_categorie ),
	CONSTRAINT unq_libelle_categorie UNIQUE ( libelle_categorie ) 
);

CREATE  TABLE commission ( 
	id_commission        smallserial  NOT NULL  ,
	pourcentage          double precision  NOT NULL  ,
	date_debut           timestamp DEFAULT CURRENT_DATE NOT NULL  ,
	CONSTRAINT pk_commission PRIMARY KEY ( id_commission )
);

CREATE  TABLE duree_enchere ( 
	id_duree_enchere     smallserial  NOT NULL  ,
	duree_min            double precision  NOT NULL  ,
	duree_max            double precision  NOT NULL  ,
	date_debut           timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL  ,
	CONSTRAINT pk_duree_enchere PRIMARY KEY ( id_duree_enchere )
);

CREATE  TABLE enchere ( 
	id_enchere           serial  NOT NULL  ,
	date_debut           timestamp DEFAULT CURRENT_DATE NOT NULL  ,
	date_fin             timestamp  NOT NULL  ,
	id_categorie         smallint  NOT NULL  ,
	id_user              integer  NOT NULL  ,
	montant_debut_enchere double precision  NOT NULL  DEFAULT 0  CHECK (montant_debut_enchere >= 0)  ,
	caption              text    ,
	commission           double precision  NOT NULL  ,
	CONSTRAINT pk_enchere PRIMARY KEY ( id_enchere ),
	CONSTRAINT fk_enchere_categorie FOREIGN KEY ( id_categorie ) REFERENCES categorie( id_categorie ) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE  TABLE photo_enchere ( 
	id_enchere           integer  NOT NULL  ,
	photo                bytea  NOT NULL  ,
	CONSTRAINT fk_photo_enchere_enchere FOREIGN KEY ( id_enchere ) REFERENCES enchere( id_enchere ) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE  TABLE type_user ( 
	id_type_user         smallint  NOT NULL  ,
	libelle_type_user    varchar(5)  NOT NULL  ,
	CONSTRAINT pk_type_user PRIMARY KEY ( id_type_user )
);

CREATE  TABLE users ( 
	id_user              serial  NOT NULL  ,
	id_type_user         smallint  NOT NULL  ,
	username             varchar(35)  NOT NULL  ,
	"password"           varchar(36)  NOT NULL  ,
	solde                double precision    ,
	token                varchar(36)    ,
	date_expiration_token timestamp    ,
	validite_token       boolean    ,
	date_inscription     timestamp DEFAULT CURRENT_TIMESTAMP   ,
	CONSTRAINT pk_tbl PRIMARY KEY ( id_user ),
	CONSTRAINT fk_users_type_user FOREIGN KEY ( id_type_user ) REFERENCES type_user( id_type_user ) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE  TABLE demande_recharge ( 
	id_demande           serial  NOT NULL  ,
	id_user              integer  NOT NULL  ,
	montant              double precision  NOT NULL  DEFAULT 0,
	date_validation      timestamp    ,
	CONSTRAINT pk_demande_recharge PRIMARY KEY ( id_demande ),
	CONSTRAINT fk_demande_recharge_users FOREIGN KEY ( id_user ) REFERENCES users( id_user ) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE  TABLE mise ( 
	id_mise              serial  NOT NULL  ,
	id_enchere           integer  NOT NULL  ,
	id_user              integer  NOT NULL  ,
	montant              double precision  NOT NULL  DEFAULT 0,
	date_mise            timestamp DEFAULT CURRENT_DATE NOT NULL  ,
	CONSTRAINT pk_mise PRIMARY KEY ( id_mise ),
	CONSTRAINT fk_mise_enchere FOREIGN KEY ( id_enchere ) REFERENCES enchere( id_enchere ) ON DELETE CASCADE ON UPDATE CASCADE ,
	CONSTRAINT fk_mise_users FOREIGN KEY ( id_user ) REFERENCES users( id_user ) ON DELETE CASCADE ON UPDATE CASCADE 
);

--
-- FUNCTIONS
--
CREATE OR REPLACE FUNCTION generate_token()
	RETURNS trigger
 	LANGUAGE plpgsql
AS $function$
BEGIN
		UPDATE users SET validite_token = TRUE, date_expiration_token = CURRENT_TIMESTAMP + (168 * interval '1 hour') WHERE id_user = NEW.id_user;
    RETURN NEW;
END;
$function$
;

CREATE TRIGGER user_token_seq AFTER UPDATE OF token ON users FOR EACH ROW EXECUTE FUNCTION generate_token();
