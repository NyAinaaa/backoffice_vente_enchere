INSERT INTO type_user VALUES
    (1,'admin'),
    (2,'user')
;

INSERT INTO users (id_type_user, username, "password", solde) VALUES
    (1, 'Joujou', MD5('root'), 300000),
    (2, 'Miaa', MD5('root'), 100000),
    (2,'NyAina', MD5('root'), 1000000),
    (2, 'Mahefa Nant', MD5('root'), 500000)
;

INSERT INTO categorie (libelle_categorie) VALUES
    ('Ameublement'),
    ('Art et decoration'),
    ('Mode et bijoux'),
    ('Informatique et telephonie'),
    ('Instruments de musique'),
    ('Jouets'),
    ('Livres et manuscrits'),
    ('Vehicules'),
    ('Objets de collection')
;

INSERT INTO commission (pourcentage, date_debut) VALUES
    (15, '2022-11-01 09:00:00'),
    (20, '2022-12-20 08:00:00'),
    (10, '2023-01-01 08:45:00')    
;

INSERT INTO enchere (date_debut, date_fin, id_categorie, id_user, montant_debut_enchere, caption, commission) VALUES
    ('2023-01-17 12:00:00', '2023-01-19 12:00:00', 2, 2, 55000, 'Chaussures Nike en vente aux encheres - une occasion unique de posseder des chaussures de sport de qualite superieure pour ameliorer votre performance', 10),
    ('2023-01-17 15:00:00', '2023-01-17 21:00:00', 9, 3, 35600, 'Lumieres LED 15m en vente aux encheres - une occasion unique de posseder un eclairage decoratif et economique pour l''interieur ou l''exterieur de votre maison', 10),
    ('2023-01-17 15:00:00', '2023-01-17 21:00:00', 6, 4, 34800, 'Enchere incroyable pour une flute à bec en bois, ideale pour les debutants et les musiciens experimentes', 10),
    ('2022-12-01 00:00:00', '2022-12-02 00:00:00', 1, 1, 100000.0, 'Chaise en vente aux encheres - une occasion unique de posseder un meuble de design classique', 15),
    ('2022-12-03 00:00:00', '2022-12-04 00:00:00', 2, 2, 50000.0, 'Lampe en vente aux encheres - une piece de collection rare et elegante pour eclairer votre interieur', 15),
    ('2022-12-05 00:00:00', '2022-12-06 00:00:00', 3, 3, 10000.0, 'Bracelet en vente aux encheres - une piece de bijouterie unique et precieuse pour completer votre style', 15),
    ('2022-12-07 00:00:00', '2022-12-08 00:00:00', 4, 4, 900000.0, 'Moniteur en vente aux encheres - une occasion rare de posseder un ecran haute resolution pour ameliorer votre experience visuelle', 15),
    ('2022-12-09 00:00:00', '2022-12-10 00:00:00', 5, 1, 2000000.0, 'Guitare Stratocaster en vente aux encheres - une occasion unique de posseder un instrument de qualite professionnelle pour les amateurs de musique', 15),
    ('2022-12-11 00:00:00', '2022-12-12 00:00:00', 6, 2, 10000.0, 'Petite voiture en vente aux encheres - une occasion rare de posseder un vehicule fiable et economique pour vos deplacements quotidiens', 15),
    ('2022-12-13 00:00:00', '2022-12-14 00:00:00', 7, 3, 30000.0, 'Un grand vase en porcelaine et floral - ne ratez pas cette occasion de posseder une telle chose pour decorer votre maison', 15),
    ('2022-12-15 00:00:00', '2022-12-16 00:00:00', 8, 4, 125000000.0, 'Hyundai Venue en vente aux encheres - une occasion rare de posseder un vehicule compact, elegant et economique pour vos deplacements quotidiens', 15),
    ('2022-12-17 00:00:00', '2022-12-18 00:00:00', 9, 1, 200000.0, 'Cartes Yu-Gi-Oh rares en vente aux encheres - une occasion unique de posseder des pieces de collection pour les amateurs de jeux de cartes', 15),
    ('2022-12-19 00:00:00', '2022-12-20 00:00:00', 1, 2, 170000.0, 'Matelas gonflable en vente aux encheres - une occasion rare de posseder un lit confortable et pratique pour les vacances ou les invites', 15),
    ('2022-12-21 00:00:00', '2022-12-22 00:00:00', 2, 3, 30000.0, 'Une bouteille de vin rouge de Suresne - une occasion pour vous de gouter du vin de l`annee 1994', 20),
    ('2022-12-23 00:00:00', '2022-12-24 00:00:00', 3, 4, 80000.0, 'Montre en vente aux encheres - une occasion rare de posseder une piece de temps de qualite superieure pour completer votre style et votre precision', 20),
    ('2022-12-25 00:00:00', '2022-12-26 00:00:00', 4, 1, 5500000.0, 'Iphone 13 Pro Max en vente aux encheres - une occasion unique de posseder un smartphone haut de gamme avec les dernieres fonctionnalites et technologies de la marque Apple', 20),
    ('2022-12-27 00:00:00', '2022-12-28 00:00:00', 5, 2, 15000.0, 'Cordes pour violon en vente aux encheres - une occasion rare de posseder un accessoire de qualite pour les musiciens professionnels ou amateur', 20),
    ('2023-01-01 00:00:00', '2023-01-02 00:00:00', 6, 1, 400000.0, 'Maison de couple Barbie meublee en vente aux encheres - une occasion unique de posseder un jouet de collection pour les enfants ou les amateurs de poupees', 10),
    ('2023-01-03 00:00:00', '2023-01-04 00:00:00', 2, 2, 60000.0, 'Tableau mural 30x30cm en vente aux encheres - une occasion unique de posseder une piece d''art originale pour decorer votre interieur', 10),
    ('2023-01-05 00:00:00', '2023-01-06 00:00:00', 3, 3, 20000.0, 'Casquette en vente aux encheres - une occasion rare de posseder un accessoire de mode tendance pour completer votre look', 10),
    ('2023-01-07 00:00:00', '2023-01-08 00:00:00', 4, 4, 80000.0, 'Un Souris de marque ASUS - une occasion pour vous d`utiliser un souris de gamme pour une performance de jeux videos', 10),
    ('2023-01-09 00:00:00', '2023-01-10 00:00:00', 5, 1, 520000.0, 'Enchere en cours pour un clavier Vista - ne ratez pas cette occasion de posseder un morceau de l''histoire informatique', 10),
    ('2023-01-11 00:00:00', '2023-01-12 00:00:00', 6, 2, 80000.0, 'Enchere incroyable! Tentez votre chance pour cette tente de jeux pour enfant, ideale pour des heures d''amusement en toute securite', 10),
    ('2023-01-16 00:00:00', '2023-01-17 00:00:00', 1, 1, 250000.0, 'Enchere à ne pas manquer! Table de chevet en bois massif, style vintage, pour donner une touche unique à votre chambre à coucher', 10),
    ('2023-01-18 00:00:00', '2023-01-19 00:00:00', 2, 2, 10000.0, 'Enchere en cours pour un magnifique cadre photo 33x43cm, ideal pour mettre en valeur vos souvenirs les plus precieux', 10),
    ('2023-01-20 00:00:00', '2023-01-21 00:00:00', 3, 3, 50000.0, 'Enchere en cours pour un crop top Shein tendance et confortable, parfait pour completer votre garde-robe estivale', 10),
    ('2023-01-22 00:00:00', '2023-01-23 00:00:00', 4, 4, 100000.0, 'Enchere en cours pour un combo clavier et souris AOC fiable et ergonomique, ideal pour ameliorer votre experience de bureau', 10),
    ('2023-01-24 00:00:00', '2023-01-25 00:00:00', 5, 1, 20000.0, 'Une ancienne boite de jeu de dame - une occasion unique de posseder des pieces de collection pour les amateurs de jeux de dame', 10),
    ('2023-01-26 00:00:00', '2023-01-27 00:00:00', 6, 2, 45000.0, 'Enchere en cours pour un set Lego Transformers, ideal pour les amateurs de jeux de construction et de la franchise Transformers', 10),
    ('2023-01-28 00:00:00', '2023-01-29 00:00:00', 7, 3, 60000.0, 'Enchere en cours pour un dictionnaire Anglais-Anglais Oxford, ideal pour les etudiants, les professionnels et les amoureux de la langue anglaise', 10),
    ('2023-01-30 00:00:00', '2023-01-31 00:00:00', 8, 4, 129500000.0, 'Enchere à ne pas manquer! Mazda CX-3 annee 2016, avec faible kilometrage et entretien regulier, ideale pour les conducteurs à la recherche d''un vehicule fiable et elegant', 10),
    ('2023-02-01 00:00:00', '2023-02-02 00:00:00', 9, 1, 16000000.0, 'Enchere en cours pour un vin rouge Romanee-Conti, une rarete pour les amateurs de vin et les collectionneurs', 10),
    ('2023-02-03 00:00:00', '2023-02-04 00:00:00', 1, 2, 350000.0, 'Enchere en cours pour un canape en cuir de haute qualite, confortable et elegant, ideal pour ajouter une touche de sophistication à votre salon.', 10)
;

INSERT INTO mise(id_enchere, id_user, montant, date_mise) VALUES 
    (22, 2, 530000, '2023-01-09 09:00:00'),
    (22, 3, 550000, '2023-01-09 10:00:00'),
    (22, 2, 560000, '2023-01-09 18:00:00'),
    (22, 4, 600000, '2023-01-11 12:00:00'),
    (22, 2, 640000, '2023-01-15 06:30:00'),
    (23, 4, 85000, '2023-01-15 05:05:05'),
    (23, 1, 100000, '2023-01-16 15:05:05'),
    (23, 4, 105000, '2023-01-16 20:05:05'),
    (23, 3, 135000, '2023-01-16 21:05:05'),
    (23, 4, 155000, '2023-01-16 22:05:05'),
    (24, 3, 260000, '2023-01-16 23:23:23'),
    (24, 4, 280000, '2023-01-17 03:23:23'),
    (24, 3, 290000, '2023-01-17 13:23:23'),
    (24, 4, 300000, '2023-01-18 08:23:23'),
    (24, 3, 350000, '2023-01-19 10:23:23')
;
