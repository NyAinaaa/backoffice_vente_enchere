CREATE OR REPLACE VIEW v_enchere_categorie AS
SELECT  id_enchere,
        date_debut,
        date_fin,
        enchere.id_categorie,
        libelle_categorie,
        id_user,
        montant_debut_enchere,
        commission,
        caption
FROM
        enchere
JOIN
        categorie
ON
        enchere.id_categorie = categorie.id_categorie;

CREATE OR REPLACE VIEW v_nombre_enchere_categorie AS
SELECT  id_categorie,
        libelle_categorie,
        COALESCE(COUNT(id_enchere), 0)  AS nombre_enchere
FROM
        v_enchere_categorie
GROUP BY
        (
            id_categorie,
            libelle_categorie
        );

CREATE OR REPLACE VIEW v_enchere_terminee AS
SELECT  v_enchere_categorie.*,
        COALESCE(MAX(mise.montant), 0) prix_actuel
FROM
        v_enchere_categorie
JOIN
        mise
ON
        v_enchere_categorie.id_enchere = mise.id_enchere
GROUP BY
        (
            v_enchere_categorie.id_enchere,
            date_debut,
            date_fin,
            id_categorie,
            libelle_categorie,
            v_enchere_categorie.id_user,
            montant_debut_enchere,
            commission,
            caption
        )
HAVING
        date_fin < CURRENT_TIMESTAMP;

CREATE OR REPLACE VIEW v_revenu_categorie AS
SELECT  id_categorie,
        libelle_categorie,
        COALESCE(SUM(prix_actuel*commission/100), 0)  AS revenu
FROM
        v_enchere_terminee
GROUP BY
        (
            id_categorie,
            libelle_categorie
        );

CREATE OR REPLACE VIEW v_enchere_gagnee_categorie AS
SELECT  id_categorie,
        libelle_categorie,
        COALESCE(COUNT(id_enchere), 0)  AS enchere_gagnee
FROM
        v_enchere_terminee
GROUP BY
        (
            id_categorie,
            libelle_categorie,
            prix_actuel
        )
HAVING
        v_enchere_terminee.prix_actuel > 0;

CREATE OR REPLACE VIEW v_enchere_perdue_categorie AS
SELECT  id_categorie,
        libelle_categorie,
        COALESCE(COUNT(id_enchere), 0)  AS enchere_perdue
FROM
        v_enchere_terminee
GROUP BY
        (
            id_categorie,
            libelle_categorie,
            prix_actuel
        )
HAVING
        v_enchere_terminee.prix_actuel = 0;

CREATE OR REPLACE VIEW v_statistique_categorie AS
SELECT  v_nombre_enchere_categorie.*,
        v_revenu_categorie.revenu,
        v_enchere_gagnee_categorie.enchere_gagnee,
        v_enchere_perdue_categorie.enchere_perdue,
        enchere_gagnee/nombre_enchere  AS win_rate,
        enchere_perdue/nombre_enchere  AS loss_rate
FROM
        v_nombre_enchere_categorie
JOIN
        v_revenu_categorie
ON
        v_nombre_enchere_categorie.id_categorie = v_revenu_categorie.id_categorie
JOIN
        v_enchere_gagnee_categorie
ON
        v_enchere_gagnee_categorie.id_categorie = v_revenu_categorie.id_categorie
JOIN
        v_enchere_perdue_categorie
ON
        v_enchere_gagnee_categorie.id_categorie = v_enchere_perdue_categorie.id_categorie
ORDER BY
        (
            revenu,
            enchere_gagnee/nombre_enchere,
            enchere_gagnee,
            enchere_perdue/nombre_enchere,
            enchere_gagnee,
            nombre_enchere
        )
DESC;
